
# README #

This app takes URLs from [_src/input.json_](src/input.json), runs some [Lighthouse](https://github.com/GoogleChrome/lighthouse#using-the-node-cli) audits, and outputs scores to [_output.csv_](src/output.csv).

This has been **heavily** lifted from this work from [Multihouse](https://github.com/samdutton/multihouse) by Sam Dutton. By 'lifted', I mean, it's pretty much exactly the same, except this has been slightly modified to:
* Use a json formatted input of sites to test
* Only output the results for *Performance* and *Accessibility* tests
* Append a date to the results

### But why? ###

* Because accessibility is important
* So that multiple sites can be audited quickly
* To facilitate better representation of how our sites stack up, and to track changes in scores

### How do I get my site tested too? ###

Each entry in [_src/input.json_](src/input.json) consists of a site name, a page type and a URL.

For example:

```
{
	"name": "World Rugby",
	"page": "Home Page",
	"url": "https://www.world.rugby"
}
```
To add another entry to the list of sites to test, create a PR editing the `input.json` file with your site added in the same format as all the others. It'll then get picked up as part of the run. 

Currently, the `page` value has no bearing on anything, and this whole thing is focussed on the expectation that there will only be 1 entry per site, and that'll be the Home Page. *Better handling of different pages from the same site to come(?)*  👀

### What happens to the results? ###

Currently, I (Matt N) will just be running this manually (once a week? fortnight? 🤷) and copying the scores over to a google sheet called [a11y scores](https://docs.google.com/spreadsheets/d/1FwyjpEkLIskDdc_GMncLHrJkbSXF11rSmXmswxqbM_w/edit?usp=sharing). Then through the magic of pivot tables, that sheet will produce some charts to show the changes of scores for each site, as well as a snapshot of how each site ranks against each other.

Longterm, I'd like this to be much less manual, and have some nicer charts and stuff, but this'll do for the moment.

**This data is for internal use only, and should be seen as a relatively harmless bit of fun, albeit with actual serious consequences.** By which I mean, the scores are merely prompts to review how each site is performing, and if you're in a position to make improvements, you should! 💪🏻 Notable improvements or consistently high scores may get highlighted in other places, as will particularly low scores, but this is all in the interest of being better across all projects, it's not intended to point fingers.

#### To run it yourself... ####

You don't *need* to, but if you want to run this locally, you can! Do note, this only works with publicly available urls but still, it may be useful if you want to take your own snapshots or look at multiple different pages (away from the Home Page) on a site.

1. Clone the code or download it as a zip.
2. From a terminal window, go to to the `a11y-auditing` directory you created and run `npm install` to install the required Node modules.
3. Check URLs to be audited in [_input.json_](src/input.json), and add your own if required as described above.
4. From a terminal `cd` to the `src` directory and run `node index.js`.
5. Progress updates and errors will be logged to the console.
6. When all Lighthouse runs are complete, view the results in [_output.csv_](src/output.csv).
7. Check for errors in _error-log.txt_.